"""Console script for ec2_minecraft."""
import argparse
import os
import sys
import time

from .ec2_minecraft import (
    getip,
    start,
    stop,
)
ACTION = { 'getip': getip,
           'start': start, 
           'stop': stop }


def argument_parser(arguments):
    parser = argparse.ArgumentParser(
        description='Start EC2 instance')
    parser.add_argument('action', choices=['start', 'stop', 'getip'])
    parser.add_argument('--instance', help='Instance ID (default: $EC2MCINSTANCE)', default=os.environ.get('EC2MCINSTANCE', None))
    return parser


def parse_arguments(arguments):
    parser = argument_parser(arguments)
    args = parser.parse_args(arguments)
    if args.instance is None:
        sys.exit(parser.print_usage())
    return args


def main():
    """Console script for ec2_minecraft."""
    args = parse_arguments(sys.argv[1:])
    action = ACTION[args.action]
    out, err = action(args.instance)
    sys.stdout.write(out)
    sys.stderr.write(err)


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
