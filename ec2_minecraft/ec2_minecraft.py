"""Main module."""
from .constants import PORTMC
from subprocess import Popen, PIPE

def getip(instance):
    args = ['aws', 'ec2', 'describe-instances', '--instance-ids', instance]
    proc = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    out, err = proc.communicate()
    addrs = []
    for l in out.split('\n'):
        if l.startswith('ASSOCIATION'):
            ip = l.split()[3]
            addr = f'{ip}:{PORTMC}'
            if addr not in addrs:
                addrs.append(addr)
    outs = '\n'.join(addrs) + '\n'
    return outs, err
    

def start(instance):
    args = ['aws', 'ec2', 'start-instances', '--instance-ids', instance]
    proc = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    out, err = proc.communicate()
    return out, err
    

def stop(instance):
    args = ['aws', 'ec2', 'stop-instances', '--instance-ids', instance]
    proc = Popen(args, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    out, err = proc.communicate()
    return out, err
    

