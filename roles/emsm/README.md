# msm

Minecraft on Amazon EC2 using Minecraft Server Manager

## Usage

`ansible-galaxy install msm`

LICENSE: 3-clause BSD license.

## CONTRIBUTING

`git clone git@github.com:jfriis/ec2_minecraftXXX`

---
Copyright © 2020, Jens Friis-Nielsen
