#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo -e "Usage:\n$0 WORLD" >&2
    exit 254
fi

WORLD=$1
CONNECTED=$(timeout 2 minecraft -w $WORLD worlds --verbose-send list | grep ']: There are ' | tail -1 | sed -r 's/.+ There are //' | sed -r 's/ .+//')

# test if output is an integer
case $CONNECTED in
    ''|*[!0-9]*)
        echo "Number of connected not an integer: '$CONNECTED'" >&2
        exit 0  # useful to stop-instance with command: connected && stop-instance, stops on errors
        ;;
    *)
        echo $CONNECTED
        exit $CONNECTED
        ;;
esac
