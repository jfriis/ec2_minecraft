=====
Usage
=====


ec2mc
=====

To use EC2 Minecraft:

Start instance::

    ec2mc start INSTANCE-ID

Get IP of an instance::

    ec2mc getip INSTANCE-ID

Stop instance::

    ec2mc stop INSTANCE-ID
