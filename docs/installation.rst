.. highlight:: shell

============
Installation
============


ec2_minecraft
=============


Stable release
--------------

To install EC2 Minecraft, run this command in your terminal:

.. code-block:: console

    pip install ec2_minecraft

This is the preferred method to install EC2 Minecraft, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for EC2 Minecraft can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    git clone git://gitlab.com/jfriis/ec2_minecraft

Or download the `tarball`_:

.. code-block:: console

    curl -OJL https://gitlab.com/jfriis/ec2_minecraft/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    python setup.py install


.. _Gitlab repo: https://gitlab.com/jfriis/ec2_minecraft
.. _tarball: https://gitlab.com/jfriis/ec2_minecraft/tarball/master


AWS CLI
-------

EC2 Minecraft requires that the AWS command line interface is configured.

Configure::

    aws configure



Minecraft deployment
====================

Minecraft server is deployed using Ansible_ and `Minecraft Server Manager`_.

.. _`Ansible`: https://docs.ansible.com/ansible/latest/index.html

.. _`Minecraft Server Manager`: http://msmhq.com


Prerequisites
-------------

One or more AWS EC2 instances must be available through the `aws console`_.

.. _`aws console`: https://aws.amazon.com/console/


The ansible inventory should be `generated dynamically` using :code:`ec2.py` (it's more easy).

.. _`generated dynamically`: https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html#inventory-script-example-aws-ec2

.. code::

    curl -O https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.py
    curl -O https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.ini
    
You may need to disable a few services from AWS in :code:`ec2.ini` for :code:`ec2.py` to work properly:

Set :code:`elasticache = False` and :code:`rds = False`.

.. seealso::
   https://github.com/ansible/ansible/issues/10840#issuecomment-226155731

You also need permission on AWS to describe your various instances, for instance the AWS security group: AmazonEC2FullAccess.


If you add your AWS private key to :code:`ansible.cfg` you don't have to remember to use it:

.. code::

   [defaults]
   inventory = ./ec2.py
   remote_user = ubuntu
   private_key_file = /path/to/your/identity.pem


Remember that your instance must have the correct open port for minecraft (security group).


Deployment
----------

It should be as easy as:

.. code::

   ansible-playbook emsm.yml


Server management site
======================

Allow your buddies to start the server from a browser.
Deploy a flask + gunicorn site to a pre-existing nginx site.
Expects a preconfigured nginx service.
Setup HTTP basic auth for you flask site so not just everyone (or robots) can activate your instance.

Configure inventory variables

.. code:: ini

   instance_id: ''  # aws ec2 instance-id
   flask_dir: /var/www/ec2mc
   flask_owner: "{{ ansible_user }}"
   fqdn: ec2mc.example.com
   basic_auth_user: mc
   basic_auth_password: ''
   credential_store: '{{ playbook_dir }}/credential_store/ec2mc'

Deploy

.. code::

   ansible-playbook www.yml

