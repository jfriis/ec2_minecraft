=============
EC2 Minecraft
=============


.. image:: https://img.shields.io/pypi/v/ec2_minecraft.svg
        :target: https://pypi.python.org/pypi/ec2_minecraft

.. image:: https://img.shields.io/travis/jensfriisnielsen/ec2_minecraft.svg
        :target: https://travis-ci.com/jensfriisnielsen/ec2_minecraft

.. image:: https://readthedocs.org/projects/ec2-minecraft/badge/?version=latest
        :target: https://ec2-minecraft.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Minecraft on Amazon EC2 using Minecraft Server Manager


* Free software: GNU General Public License v3
* Documentation: https://ec2-minecraft.readthedocs.io.


Features
--------

* Start, stop and get the IP of an amazon EC2 instance.

Planned:

- Deployment of minecraft server and utilities using Ansible.
- Configuration of EC2 instance using Ansible.
- Webserver for low-skill management of EC2 instance.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
