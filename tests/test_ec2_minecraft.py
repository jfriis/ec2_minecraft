#!/usr/bin/env python

"""Tests for `ec2_minecraft` package."""

import pytest


from ec2_minecraft import ec2_minecraft


@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # from bs4 import BeautifulSoup
    # assert 'Gitlab' in BeautifulSoup(response.content).title.string
